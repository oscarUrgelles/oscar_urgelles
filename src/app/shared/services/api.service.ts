import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';

import { environment } from '../../../environments/environment';

@Injectable()
export class ApiService {

  public responseCache = new Map();

  constructor(private http: HttpClient) { }

  /**
   * Get Players from api
   */
  getPlayers(): Observable<any> {
    return this.http.get(environment.playersUrl);
  }


  /**
  * Get Teams from api, and cache response
  */
  getTeams(): Observable<any> {
    const teamsFromCache = this.responseCache.get(environment.teamsUrl);
    if (teamsFromCache) {
      return of(teamsFromCache);
    }
    const response = this.http.get(environment.teamsUrl);
    response.subscribe(teams => this.responseCache.set(environment.teamsUrl, teams));
    return response;
  }

  /**
   * Get Streaks from api
   */
  getStreaks(): Observable<any> {
    return this.http.get(environment.streaksUrl);
  }


}
