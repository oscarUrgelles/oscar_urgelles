export const environment = {
  production: true,
  teamsUrl: 'http://localhost:3000/teams',
  playersUrl: 'http://localhost:3000/players',
  streaksUrl : 'http://localhost:3000/streaks'
};
