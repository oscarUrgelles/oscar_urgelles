import { ComponentFixture, TestBed } from '@angular/core/testing';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { configureTestSuite } from '../../../../assets/test/testing';
import { PLAYER_DIALOG_MOCK } from '../../../shared/mocks/players.mock';

import { PlayerInfoDialogComponent } from './player-info-dialog.component';

describe('PlayerInfoDialogComponent', () => {
  let component: PlayerInfoDialogComponent;
  let fixture: ComponentFixture<PlayerInfoDialogComponent>;

  const dialogMock = {
    close: () => { }
  };

  configureTestSuite();

  beforeAll(done => (async () => {
    TestBed.configureTestingModule({
      imports: [

      ],
      declarations: [
        PlayerInfoDialogComponent,
      ],
      providers: [
        { provide: MatDialogRef, useValue: dialogMock },
        { provide: MAT_DIALOG_DATA, useValue: PLAYER_DIALOG_MOCK },

      ]
    }).compileComponents();
  })().then(done).catch(done.fail));

  beforeEach(() => {
    fixture = TestBed.createComponent(PlayerInfoDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should call to the close dialog ', () => {

    const spy = spyOn(component.dialogRef, 'close').and.callThrough();

    component.ngOnInit();

    component.close();

    expect(spy).toHaveBeenCalled();
  });



});
