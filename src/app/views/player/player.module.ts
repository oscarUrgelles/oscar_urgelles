import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PlayerInfoDialogComponent } from './player-info-dialog/player-info-dialog.component';
import { MaterialModule } from '../../material/material.module';

@NgModule({
  imports: [
    CommonModule,
    MaterialModule
  ],
  declarations: [PlayerInfoDialogComponent],
  exports: [
    PlayerInfoDialogComponent
  ]
})
export class PlayerModule { }
