import { SelectionModel } from '@angular/cdk/collections';
import { Component, EventEmitter, Input, OnInit, Output, ViewChild } from '@angular/core';
import { MatPaginator, MatTableDataSource, PageEvent } from '@angular/material';
import { Pagination, Team } from '../../../../../shared/entities';

@Component({
  selector: 'app-teams-table',
  templateUrl: './teams-table.component.html',
  styleUrls: ['./teams-table.component.scss']
})
export class TeamsTableComponent {

  /**
   * Contains the columns to displa
   */
  @Input() displayedColumns: string[];

  /**
   * Recibes information about pagination
   */
  @Input() pagination: Pagination;

  /**
   * paired operations ready to display in the table
   */
  @Output() updateTableEvent: EventEmitter<any> = new EventEmitter<any>();

    /**
   * paired operations ready to display in the table
   */
  @Output() selectTeamEvent: EventEmitter<any> = new EventEmitter<any>();


  /**
   * Table checkbox selection elements
   */
  @Input() selection: SelectionModel<Team>;

  @ViewChild('matPaginator') paginator: MatPaginator;
  dataSource: MatTableDataSource<any>;

  constructor() {
  }

  /**
   * Issues an event to update the pagination-related variables in the parent component
   * @param event mat-paginator information
   */
  updateTable(event: PageEvent): void {
    this.updateTableEvent.emit(event);
  }

  /**
   * This method emit the team clicked to parent.
   * @param event Team clicked.
   */
  rowClicked(event) {
    this.selectTeamEvent.emit(event.id);
  }
}

