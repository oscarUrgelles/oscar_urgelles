import { Component, Inject, OnInit } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { Player } from '../../../shared/entities';

@Component({
  selector: 'app-player-info-dialog',
  templateUrl: './player-info-dialog.component.html',
  styleUrls: ['./player-info-dialog.component.scss']
})
export class PlayerInfoDialogComponent implements OnInit {

  /**
   * this parameter represents a player
   */
  public player: Player;

  constructor(
    @Inject(MAT_DIALOG_DATA) public data: any,
    public dialogRef: MatDialogRef<PlayerInfoDialogComponent>,
  ) { }

  ngOnInit() {
    this.player = this.data.player;
  }

  /**
   * This method close the modal
   */
  close() {
    this.dialogRef.close();
  }

}
