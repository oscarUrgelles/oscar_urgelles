import { HttpClientTestingModule } from '@angular/common/http/testing';
import { NO_ERRORS_SCHEMA } from '@angular/core';
import { ComponentFixture, inject, TestBed } from '@angular/core/testing';
import { BrowserDynamicTestingModule } from '@angular/platform-browser-dynamic/testing';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ActivatedRoute } from '@angular/router';
import { RouterTestingModule } from '@angular/router/testing';
import { of } from 'rxjs';
import { configureTestSuite } from '../../../../assets/test/testing';
import { MaterialModule } from '../../../material/material.module';
import { ApiServiceMock } from '../../../shared/mocks/api.mock.service';
import { PLAYERS } from '../../../shared/mocks/players.mock';
import { ApiService } from '../../../shared/services/api.service';
import { PlayerInfoDialogComponent } from '../../player/player-info-dialog/player-info-dialog.component';
import { TeamsModule } from '../teams.module';

import { TeamComponent } from './team.component';

let activateRouterMock = {
  params: of({ id: 1 }),
  data: of({ data: '' }),
  root: {
    routeConfig: {
      data: {
        breadcrumb: 'a'
      },
      path: '/team'
    }
  }
};

describe('TeamComponent', () => {
  let component: TeamComponent;
  let fixture: ComponentFixture<TeamComponent>;

  configureTestSuite();

  beforeAll(done => (async () => {
    TestBed.configureTestingModule({
      imports: [
        MaterialModule,
        TeamsModule,
        HttpClientTestingModule,
        RouterTestingModule,
        BrowserAnimationsModule],
      providers: [
        { provide: ApiService, useClass: ApiServiceMock },
        {
          provide: ActivatedRoute,
          useValue: activateRouterMock
        }
      ],
      declarations: [],
      schemas: [NO_ERRORS_SCHEMA]
    })
    .overrideModule(BrowserDynamicTestingModule, {
      set: { entryComponents: [PlayerInfoDialogComponent] }
    })
      .compileComponents();
  })().then(done).catch(done.fail));

  beforeEach(() => {
    fixture = TestBed.createComponent(TeamComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should goHome', () => {
    component.goHome();
  });

  it('should Get No datA', () => {
    activateRouterMock = {
      params: of({ id: 23 }),
      data: of({ data: '' }),
      root: {
        routeConfig: {
          data: {
            breadcrumb: 'a'
          },
          path: '/team'
        }
      }
    };

    TestBed.compileComponents();
    fixture = TestBed.createComponent(TeamComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
    expect(component).toBeTruthy();
  });

  it('should open dialog ', () => {
    component.moreInfo(PLAYERS.players[1]);
  });

  it('should getPlayers branch', inject([ApiService], (service: ApiService) => {
    spyOn(service, 'getPlayers').and.returnValue(of([]));
    component.getPlayers(1);
  }));

  it('should getTeamName branch ', inject([ApiService], (service: ApiService) => {
    spyOn(service, 'getTeams').and.returnValue(of([]));
    component.getTeamName(1);
  }));

});
