import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { MainTeamsComponent } from './main-teams/main-teams.component';
import { TeamComponent } from './team/team.component';

const routes: Routes = [
  {
    path: '',
    children: [
      {
        path: '',
        component: MainTeamsComponent,
      },
      {
        path: 'team/:id',
        component: TeamComponent,
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class TeamsRoutingModule { }
