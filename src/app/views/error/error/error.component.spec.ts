import { ComponentFixture, TestBed } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { configureTestSuite } from '../../../../assets/test/testing';

import { ErrorComponent } from './error.component';


let component: ErrorComponent;
  let fixture: ComponentFixture<ErrorComponent>;

describe('ErrorComponent', () => {
  configureTestSuite();

  beforeAll(done => (async () => {
    TestBed.configureTestingModule({
      declarations: [
        ErrorComponent
      ],
      imports: [
        RouterTestingModule
      ],
      providers: [
      ]
    })
      .compileComponents();
  })().then(done).catch(done.fail));

  beforeEach(() => {
    fixture = TestBed.createComponent(ErrorComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should goHome', () => {
    component.goHome();
  });

});
