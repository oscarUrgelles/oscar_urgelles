
import { Component, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { MatTableDataSource, PageEvent } from '@angular/material';
import { Router } from '@angular/router';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { Pagination, Team } from '../../../shared/entities';
import { ApiService } from '../../../shared/services/api.service';
import { TeamsTableComponent } from './components/teams-table/teams-table.component';

@Component({
  selector: 'app-main-teams',
  templateUrl: './main-teams.component.html',
  styleUrls: ['./main-teams.component.scss']
})
export class MainTeamsComponent implements OnInit, OnDestroy {
  /**
   * Columns to display
   */
  public displayedColumns: string[] = [
    'name'
  ];

  /**
   * Contains all Teams.
   */
  private teams: Team[];

  /**
   * contains filtered teams.
   */
  private filteredTeams: Team[];

  /**
  * Contain pagination data
  */
  pagination: Pagination = { page: 0, size: 10 };

  @ViewChild(TeamsTableComponent) child: TeamsTableComponent;

  private destroy$: Subject<void> = new Subject<void>();

  constructor(
    private apiService: ApiService,
    private router: Router
  ) { }

  ngOnInit() {
    this._getTeams();
  }

  /**
   * this method navigate to team ID recived.
   * @param event Team Selected
   */
  selectTeam(event) {
    this.router.navigate(['team', event]);
  }

  /**
   * this method filter the teams to be displayed at the table.
   * @param event pageEvent
   */
  changePage(event: PageEvent) {
    this.pagination = { page: event.pageIndex, size: event.pageSize };
    this.filteredTeams = this.teams.slice(this.pagination.page * this.pagination.size,
      (this.pagination.page * this.pagination.size) + this.pagination.size);
    this.child.dataSource = new MatTableDataSource<Team>(this.filteredTeams);
    this.pagination.totalElements = this.teams.length;
  }

  /**
   * Subscribe to the get TEAMS observable and set the teams to display at the table, page param's component props
   */
  private _getTeams() {
    this.apiService.getTeams().pipe(
      takeUntil(this.destroy$)
    ).subscribe(res => {
      this.teams = res;
      this.filteredTeams = this.teams.slice(0, 10);
      this.child.dataSource = new MatTableDataSource<Team>(this.filteredTeams);
      this.pagination.totalElements = this.teams.length;
    });

  }

  ngOnDestroy(): void {
    this.destroy$.next();
    this.destroy$.complete();
  }

}
