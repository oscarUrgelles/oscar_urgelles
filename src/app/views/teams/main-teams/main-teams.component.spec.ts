import { ComponentFixture, TestBed } from '@angular/core/testing';
import { MatButtonModule, MatCardModule, MatDialogModule, MatTableModule, PageEvent } from '@angular/material';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { Router } from '@angular/router';
import { RouterTestingModule } from '@angular/router/testing';
import { configureTestSuite } from '../../../../assets/test/testing';
import { ApiServiceMock } from '../../../shared/mocks/api.mock.service';
import { ApiService } from '../../../shared/services/api.service';
import { TeamsModule } from '../teams.module';

import { MainTeamsComponent } from './main-teams.component';

describe('MainTeamsComponent', () => {
  let component: MainTeamsComponent;
  let fixture: ComponentFixture<MainTeamsComponent>;
  let routerSpy = {navigate: jasmine.createSpy('navigate')};

  configureTestSuite();

  beforeAll(done => (async () => {
    TestBed.configureTestingModule({
      declarations: [
      ],
      imports: [
        TeamsModule,
        MatCardModule,
        MatButtonModule,
        MatTableModule,
        MatDialogModule,
        BrowserAnimationsModule,
        RouterTestingModule
      ],
      providers: [
        { provide: ApiService, useClass: ApiServiceMock },
        { provide: Router, useValue: routerSpy }
      ]
    })
      .compileComponents();
  })().then(done).catch(done.fail));

  beforeEach(() => {
    fixture = TestBed.createComponent(MainTeamsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should select team', () => {
    component.selectTeam('1');
    expect (routerSpy.navigate).toHaveBeenCalledWith( [ 'team', '1' ] );
  });

  it('should set paginator values', () => {
    component.pagination.size = 5;
    component.pagination.page = 1;

    const paginationEvent: PageEvent = {
      pageIndex: 1,
      pageSize: 5,
      length: 8
    };

    component.changePage(paginationEvent);
    expect(component.pagination.size).toEqual(5);
    expect(component.pagination.page).toEqual(1);
  });

});
