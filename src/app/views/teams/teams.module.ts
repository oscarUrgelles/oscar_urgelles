import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { TeamsRoutingModule } from './teams-routing.module';

import { MainTeamsComponent } from './main-teams/main-teams.component';
import { TeamsTableComponent } from './main-teams/components/teams-table/teams-table.component';
import { MaterialModule } from '../../material/material.module';
import { ApiService } from '../../shared/services/api.service';
import { TeamComponent } from './team/team.component';
import { PlayerInfoDialogComponent } from '../player/player-info-dialog/player-info-dialog.component';
import { PlayerModule } from '../player/player.module';

@NgModule({
  imports: [
    CommonModule,
    TeamsRoutingModule,
    MaterialModule,
    PlayerModule
  ],
  providers: [
    ApiService
  ],
  declarations: [MainTeamsComponent, MainTeamsComponent, TeamsTableComponent, TeamComponent],
  entryComponents: [PlayerInfoDialogComponent]
})
export class TeamsModule { }
