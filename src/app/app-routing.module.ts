import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { DashboardLayoutComponent } from './layouts/dashboard-layout/dashboard-layout.component';


const routes: Routes = [
  {
    path: '',
    children: [
      {
        path: '',
        component: DashboardLayoutComponent,
        children: [
          {
            path: '',
            loadChildren: './views/teams/teams.module#TeamsModule',
          },
        ]
      },
      {
        path: 'error',
        loadChildren: './views/error/error.module#ErrorModule',
      },
      { path: '**', redirectTo: '', pathMatch: 'full' },
    ]
  }
];


@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
