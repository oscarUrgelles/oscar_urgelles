import { Component, OnDestroy, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material';
import { ActivatedRoute, Router } from '@angular/router';
import { BehaviorSubject, Subject } from 'rxjs';
import { map, takeUntil } from 'rxjs/operators';
import { Player, Team } from '../../../shared/entities';
import { ApiService } from '../../../shared/services/api.service';
import { PlayerInfoDialogComponent } from '../../player/player-info-dialog/player-info-dialog.component';

@Component({
  selector: 'app-team',
  templateUrl: './team.component.html',
  styleUrls: ['./team.component.scss']
})
export class TeamComponent implements OnInit, OnDestroy {
  /**
   * represents a id of a team
   */
  team_id: string;
  /**
   * represents a list of players
   */
  players: Player[];
  /**
   * represents a list of teams.
   */
  team: Team[];
  /**
   * represents a list of players observable
   */
  teamPlayers$: BehaviorSubject<Player[]> = new BehaviorSubject([]);

  /**
   * represents a team observable.
   */
  team$: BehaviorSubject<Team> = new BehaviorSubject(null);

  private destroy$: Subject<void> = new Subject<void>();

  constructor(
    private apiService: ApiService,
    private activatedRoute: ActivatedRoute,
    private router: Router,
    public dialog: MatDialog,
  ) { }

  ngOnInit() {
    /**
    * get the team id
    */
    this.activatedRoute.params.pipe(takeUntil(this.destroy$)).subscribe((params) => {
      this.team_id = params.id;
      this.getPlayers(parseInt(this.team_id, 10));
      this.getTeamName(parseInt(this.team_id, 10));
    });
  }

  /**
   * This method navigates to home.
   */
  goHome() {
    this.router.navigate(['']);
  }

  /**
   * This method open dialog to show info about player recived
   * @param player Player selected
   */
  moreInfo(player: Player) {
    const dialogRef = this.dialog.open(PlayerInfoDialogComponent, {
      width: '500px',
      data: {
        player
      }
    });
  }

  /**
   * This method subscribe to Teams and set the name of team recibed.
   * Este metodo se haria privado pero para este ejemplo se pone publico para poderlo testear
   * @param team_id TEAM id
   */
  getTeamName(team_id: number) {
    this.apiService.getTeams().pipe(
      map((team: Team[]) => team.filter(p => p.id === team_id)),
      takeUntil(this.destroy$)
    ).subscribe(data => {
      if (data.length) {
        this.team = data;
        this.team$.next(this.team[0]);
      } else {
        this.router.navigate(['error']);
      }
    });
  }

  /**
   * This method subscribe to Players and set the players.
   * Este metodo se haria privado pero para este ejemplo se pone publico para poderlo testear
   * @param team_id team id
   */
  getPlayers(team_id: number) {
    this.apiService.getPlayers().pipe(
      map((players: Player[]) => players.filter(p => p.id_team === team_id)),
      takeUntil(this.destroy$)
    ).subscribe(data => {
      if (data.length) {
        this.players = data;
        this.teamPlayers$.next(this.players);
      } else {
        this.teamPlayers$.next(null);
      }
    });
  }

  ngOnDestroy(): void {
    this.destroy$.next();
    this.destroy$.complete();
  }

}
