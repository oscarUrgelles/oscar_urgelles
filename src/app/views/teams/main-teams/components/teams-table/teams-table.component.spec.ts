import { ComponentFixture, TestBed } from '@angular/core/testing';
import { MatButtonModule, MatDividerModule, MatPaginatorModule, MatTableModule, PageEvent } from '@angular/material';
import { configureTestSuite } from '../../../../../../assets/test/testing';
import { Team } from '../../../../../shared/entities';
import { ApiServiceMock } from '../../../../../shared/mocks/api.mock.service';
import { ApiService } from '../../../../../shared/services/api.service';

import { TeamsTableComponent } from './teams-table.component';

describe('TeamsTableComponent', () => {
  let component: TeamsTableComponent;
  let fixture: ComponentFixture<TeamsTableComponent>;

  configureTestSuite();

  beforeAll(done => (async () => {
    TestBed.configureTestingModule({
      declarations: [TeamsTableComponent],
      imports: [
        MatButtonModule,
        MatTableModule,
        MatPaginatorModule,
        MatDividerModule,
      ],
      providers: [
        { provide: ApiService, useClass: ApiServiceMock }
      ]
    })
      .compileComponents();
  })().then(done).catch(done.fail));

  beforeEach(() => {
    fixture = TestBed.createComponent(TeamsTableComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should emit updateTableEvent event', () => {
    spyOn(component.updateTableEvent, 'emit');

    const pageEvent: PageEvent = { pageIndex: 1, pageSize: 5, length: 8 };
    component.updateTable(pageEvent);

    expect(component.updateTableEvent.emit).toHaveBeenCalled();
  });

  it('should emit selectTeamEvent event', () => {
    spyOn(component.selectTeamEvent, 'emit');

    const row: Team = {
      id: 48,
      name: 'Alavés'
    };
    component.rowClicked(row);

    expect(component.selectTeamEvent.emit).toHaveBeenCalledWith(row.id);
  });


});
