import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { TestBed, inject } from '@angular/core/testing';
import { configureTestSuite } from '../../../assets/test/testing';
import { PLAYERS, TEAMS } from '../mocks/players.mock';

import { ApiService } from './api.service';

let httpTestingController: HttpTestingController;
let service: ApiService;

describe('ApiService', () => {

  configureTestSuite();

  beforeAll(done => (async () => {
    TestBed.configureTestingModule({
      imports: [
        HttpClientTestingModule
      ],
      providers: [
        ApiService
      ]
    })
      .compileComponents();
    httpTestingController = TestBed.get(HttpTestingController);
    service = TestBed.get(ApiService);
  })().then(done).catch(done.fail));


  afterEach(() => {
    httpTestingController.verify();
  });

  it('should be created', () => {
    const service: ApiService = TestBed.get(ApiService);
    expect(service).toBeTruthy();
  });

  it(
    'should get Players',
    inject([ApiService], (service: ApiService) => {
      service.getPlayers();
    })
  );

  it(
    'should get Teams',
    inject([ApiService], (service: ApiService) => {
      service.getTeams();
    })
  );

  it('should get cached teams', inject([ApiService], (service: ApiService) => {
    service.responseCache.set('http://localhost:3000/teams', TEAMS.teams);
    service.getTeams().subscribe(res => {
      expect(res.length).toBeGreaterThan(0);
    });
  }));


  it(
    'should get Streaks',
    inject([ApiService], (service: ApiService) => {
      service.getStreaks();
    })
  );
});
