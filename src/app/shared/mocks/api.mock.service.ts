
import { of } from 'rxjs';
import { PLAYERS, TEAMS } from './players.mock';

export class ApiServiceMock {
  getPlayers() {
    return of(PLAYERS.players);
  }

  getTeams() {
    return of(TEAMS.teams);
  }
}
